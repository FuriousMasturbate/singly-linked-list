#include "linkedlist-backend.h"
#include <stdlib.h>

// internal static function prototypes:
static struct list_node*
get_address_of_node_given_its_position (struct list_node* list_head,
                                        node_index_type node_position);

static _Bool
should_traverse_to_next_node (struct list_node* node_address,
                              node_index_type index_of_desired_node);

static _Bool
not_reached_end_of_the_list (struct list_node* node_address);

static _Bool
not_reached_desired_node (struct list_node* node_address,
                          node_index_type index_of_desired_node);




void
fill_node (struct list_node* node_address,
           struct node_uid uid,
           node_data_type data)
{
	/*
	    set new_node to point to list's head.
	    (because we're adding the node to the
	    beginning of the list.)
	*/
	node_address->next = *uid.address_of_list_head;
	node_address->index = uid.index;
	node_address->data = data;
}




void
update_node (struct list_node* node_address,
             struct node_uid uid,
             node_data_type data)
{
	node_address->index = uid.index;
	node_address->data = data;
}


// external static function definitions:
void
delete_all_nodes (struct list_node* list_head)
{
	struct list_node* traversal_ptr = list_head;
	do
	{
		struct list_node* traversal_ptr_leading = traversal_ptr->next;
		free (traversal_ptr);
		traversal_ptr = traversal_ptr_leading;
	} while (traversal_ptr != NULL);
}




void
delete_non_head_node (struct node_uid uid)
{
	struct list_node* preceding_node = find_preceding_node (uid);

	if (preceding_node == NULL) return;

	struct list_node* leading_node = preceding_node->next;
	preceding_node->next = leading_node->next;
	free (leading_node);
}

struct list_node*
find_preceding_node (struct node_uid uid)
{
	struct list_node* list_head = *uid.address_of_list_head;

	struct node_properties leading_node = get_node_properties (uid);

	if (leading_node.node_address == NULL) return NULL;

	node_index_type preceding_node_position = leading_node.node_position - 1;

	return get_address_of_node_given_its_position (list_head,
	                                               preceding_node_position);
}


static struct list_node*
get_address_of_node_given_its_position (struct list_node* list_head,
                                        node_index_type node_position)
{
	struct list_node* traversal_ptr = list_head;

	while (node_position)
	{
		traversal_ptr = traversal_ptr->next;
		node_position--;
	}
	return traversal_ptr;
}


struct node_properties
get_node_properties (struct node_uid uid)
{
	struct list_node* list_head = *uid.address_of_list_head;
	struct node_properties properties = { .node_address = list_head,
		                                  .node_position = 0 };
	while (should_traverse_to_next_node (properties.node_address, uid.index))
	{
		properties.node_position++;
		properties.node_address = properties.node_address->next;
	}
	return properties;
}

static _Bool
should_traverse_to_next_node (struct list_node* node_address,
                              node_index_type index_of_desired_node)
{
	return not_reached_end_of_the_list (node_address) &&
	       not_reached_desired_node (node_address, index_of_desired_node);
}

static _Bool
not_reached_end_of_the_list (struct list_node* node_address)
{
	return node_address != NULL;
}

static _Bool
not_reached_desired_node (struct list_node* node_address,
                          node_index_type index_of_desired_node)
{
	return node_address->index != index_of_desired_node;
}
