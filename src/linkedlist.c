/*
 * singly_linked_list
 * a collection of functions for manipulating
 * singly-linked lists and utilizing heap memory area.
 *
 * Created: 2/29/2020 1:22:39 AM
 * Last modified: March 2 2020
 * Author: UniX
 */

#include "linkedlist.h"
#include "linkedlist-backend.h"
#include <stdlib.h>

_Bool list_outofmemory = 0;

// function definitions:
void
list_add_node (struct node_uid uid, node_data_type data)
{
	struct list_node* new_node = malloc (sizeof (struct list_node));
	if (new_node == NULL)
		list_outofmemory = 1;
	else
		fill_node (new_node, uid, data);
	/*
	    update list's head to point to the new node
	    that we just added to the beggining of the list:
	*/
	*uid.address_of_list_head = new_node;
	return new_node;
}




struct list_node*
list_find_node (struct node_uid uid)
{
	struct list_node* list_head = *uid.address_of_list_head;
	if (list_head == NULL) return NULL;
	return get_node_properties (uid).node_address;
}




void
list_auto_update_node (struct node_uid uid, node_data_type data)
{
	struct list_node* node = list_find_node (uid);

	if (node == NULL) // if node not found, or list does not exist:
		return list_add_node (uid, data);

	else // if node already exists:
	{
		update_node (node, uid, data);
		return node;
	}
}




void
list_destroy (struct list_node** address_of_list_head)
{
	if (*address_of_list_head == NULL) return;
	delete_all_nodes (*address_of_list_head);
	*address_of_list_head = NULL;
}




void
list_delete_node (struct node_uid uid)
{
	struct list_node* list_head = *uid.address_of_list_head;

	if (list_head == NULL) return NULL;

	if (list_head->index == uid.index) // if the node we want to delete is
	{ // the first node in the list:
		/*
		    set list's head to point to the second node
		    in the list and free the first node:
		*/
		*uid.address_of_list_head = list_head->next;
		free (list_head);
	}
	else // if the node we want to delete is NOT the first node in the list:
	{
		delete_non_head_node (uid);
	}
	return *uid.address_of_list_head;
}
