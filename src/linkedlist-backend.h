#ifndef LINKEDLIST_BACKEND_H_
#define LINKEDLIST_BACKEND_H_

#include "linkedlist.h"

extern _Bool list_outofmemory;

/*
    used in function 'find_preceding_node'
    and 'get_node_properties' in 'linkedlist_backend.c'
*/
struct node_properties
{
	struct list_node* node_address;
	node_index_type node_position; /* node_position = n, node is the
	                                  n-th element in the list */
};

// external static function prototypes:
void
fill_node (struct list_node* node_address,
           struct node_uid uid,
           node_data_type data);

void
update_node (struct list_node* node_address,
             struct node_uid uid,
             node_data_type data);

void
delete_all_nodes (struct list_node* list_head);

void
delete_non_head_node (struct node_uid uid);

struct list_node*
find_preceding_node (struct node_uid uid);

struct node_properties
get_node_properties (struct node_uid uid);

#endif /* LINKEDLIST_BACKEND_H_ */
