/*
 * singly_linked_list
 * a collection of functions for manipulating
 * singly-linked lists and utilizing heap memory area.
 *
 * Created: 2/29/2020 1:22:39 AM
 * Last modified: March 2 2020
 * Author: UniX
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_

#include <stdint.h>

#define NULL ((void*)0)

typedef uint8_t node_data_type;
typedef uint8_t node_index_type;

struct list_node
{
	node_index_type index; //"index" is used as the node's unique identifier.
	node_data_type data; //"data" is used to keep any variable in each node.
	struct list_node* next;
};

struct node_uid
{
	struct list_node** address_of_list_head;
	node_index_type index;
};

void
list_add_node (struct node_uid uid, node_data_type data);

struct list_node*
list_find_node (struct node_uid uid);

void
list_auto_update_node (struct node_uid uid, node_data_type data);

void
list_delete_node (struct node_uid uid);

void
list_destroy (struct list_node** address_of_list_head);

#endif /* LINKEDLIST_H_ */
